using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using FlockingBackend;
using System.Collections.Generic;

namespace FlockingUnitTests{
    
    [TestClass]
    public class RavenUnitTests{

        // General Tests for ChaseSparrow Method
        [TestMethod]
        [DataRow(1, 2, 3, 4, -1f)]
        [DataRow(10, 2, 3, -1, 4f)]
        [DataRow(7, -2, 4, -2.5f, 6.5f)]
        public void ChaseSparrowTests(float numOne, float numTwo, float numThree, float numFour, float expected){
            // Arrange
            Sparrow sparrowOne = new Sparrow(numOne, numOne, numOne, numOne);
            Sparrow sparrowTwo = new Sparrow(numTwo, numTwo, numTwo, numTwo);
            Sparrow sparrowThree = new Sparrow(numThree, numThree, numThree, numThree);

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            Raven raven = new Raven(numFour, numFour, numFour, numFour);

            // Act
            Vector2 newVector = raven.ChaseSparrow(flock);

            // Assert
            Assert.AreEqual(expected, newVector.Vx,  0.01);
            Assert.AreEqual(expected, newVector.Vy,  0.01);

        }
        
    }
    
}