using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using FlockingBackend;
using System.Collections.Generic;

namespace FlockingUnitTests{
    
    [TestClass]
    public class FlockUnitTests{
        
        /// <summary>
        /// Testing to see if the position of a sparrow changes after calling the invoking the events
        /// </summary>
        [TestMethod]
        public void SparrowSubscribePositionChangedTest(){
            Flock flock = new Flock();
            Sparrow sparrow = new Sparrow(1, 1, 1, 1);
            List<Sparrow> sparrows = new List<Sparrow>();
            Raven raven = new Raven(2, 2, 2, 2);
            sparrows.Add(sparrow);

            var beforeX = sparrow.Position.Vx;
            var beforeY = sparrow.Position.Vy;
            
            flock.Subscribe(sparrow.CalculateBehaviour, sparrow.Move, sparrow.CalculateRavenAvoidance);

            flock.RaiseMoveEvents(sparrows, raven);
            
            var resultX = sparrows[0].Position.Vx;
            var resultY = sparrows[0].Position.Vy;


            Assert.AreNotEqual(beforeX, resultX);
            Assert.AreNotEqual(beforeY, resultY);

        }


        /// <summary>
        /// Testing to see if the velocity of a sparrow changes after calling the invoking the events
        /// </summary>
        [TestMethod]
        public void SparrowSubscribeVelocityChangedTest(){
            Flock flock = new Flock();
            Sparrow sparrow = new Sparrow(1, 1, 1, 1);
            List<Sparrow> sparrows = new List<Sparrow>();
            Raven raven = new Raven(2, 2, 2, 2);
            sparrows.Add(sparrow);

            var beforeX = sparrow.Velocity.Vx;
            var beforeY = sparrow.Velocity.Vy;
            
            flock.Subscribe(sparrow.CalculateBehaviour, sparrow.Move, sparrow.CalculateRavenAvoidance);

            flock.RaiseMoveEvents(sparrows, raven);
            
            var resultX = sparrows[0].Velocity.Vx;
            var resultY = sparrows[0].Velocity.Vy;


            Assert.AreNotEqual(beforeX, resultX);
            Assert.AreNotEqual(beforeY, resultY);

        }

    }
    
}