using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using FlockingBackend;
using System.Linq;
using System.Collections.Generic;

namespace FlockingUnitTests{
    
    [TestClass]
    public class SparrowUnitTests{

        // General Tests for Alignment Method
        [TestMethod]
        [DataRow(1, 2, 3, 0.7f)]
        [DataRow(5, 10, 15, -0.7f)]
        public void AlignmentTests(int numOne, int numTwo, int numThree, float expected){
            // Arrange
            Sparrow sparrowOne = new Sparrow(numOne, numOne, numOne, numOne);
            Sparrow sparrowTwo = new Sparrow(numTwo, numTwo, numTwo, numTwo);
            Sparrow sparrowThree = new Sparrow(numThree, numThree, numThree, numThree);

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            // Act
            Vector2 newVector = sparrowOne.Alignment(flock);

            // Assert
            Assert.AreEqual(expected, newVector.Vx,  0.01);
            Assert.AreEqual(expected, newVector.Vy, 0.01);
        }

        // Specific Test for Alignment Method
        [TestMethod]
        public void AlignmentSpecificTestOne(){
            // Arrange
            Sparrow sparrowOne = new Sparrow(10, 5, 1, 1);
            Sparrow sparrowTwo = new Sparrow(3, 4, 2, -3);
            Sparrow sparrowThree = new Sparrow(-7, 12, 4, 0);

            float expectedX = 0.58f;
            float expectedY = -0.81f;

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            // Act
            Vector2 newVector = sparrowOne.Alignment(flock);

            // Assert
            Assert.AreEqual(expectedX, newVector.Vx, 0.01);
            Assert.AreEqual(expectedY, newVector.Vy, 0.01);
        }

        // General Tests for Cohesion Method
        [TestMethod]
        [DataRow(1, 2, 3, 0.7f)]
        [DataRow(5, 10, 15, -0.7f)]
        public void CohesionTests(int numOne, int numTwo, int numThree, float expected){
            // Arrange
            Sparrow sparrowOne = new Sparrow(numOne, numOne, numOne, numOne);
            Sparrow sparrowTwo = new Sparrow(numTwo, numTwo, numTwo, numTwo);
            Sparrow sparrowThree = new Sparrow(numThree, numThree, numThree, numThree);

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            // Act
            Vector2 newVector = sparrowOne.Cohesion(flock);

            // Assert
            Assert.AreEqual(expected, newVector.Vx, 0.01);
            Assert.AreEqual(expected, newVector.Vy, 0.01);
        }

        // Specific Test for Cohesion Method
        [TestMethod]
        public void CohesionSpecificTestOne(){
            // Arrange
            Sparrow sparrowOne = new Sparrow(1, 11, 0, 1);
            Sparrow sparrowTwo = new Sparrow(-10, 16, 1, 4);
            Sparrow sparrowThree = new Sparrow(-1, 7, 2, -3);

            float expectedX = -0.96f;
            float expectedY = -0.24f;

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            // Act
            Vector2 newVector = sparrowOne.Cohesion(flock);

            // Assert
            Assert.AreEqual(expectedX, newVector.Vx, 0.01);
            Assert.AreEqual(expectedY, newVector.Vy, 0.01);
        }

        // General Tests for Avoidance Method
        [TestMethod]
        [DataRow(1, 2, 3, -0.7f)]
        [DataRow(5, 10, 15, -0.7f)]
        public void AvoidanceTests(int numOne, int numTwo, int numThree, float expected){
            // Arrange
            World currentWorld = new World();

            Sparrow sparrowOne = new Sparrow(numOne, numOne, numOne, numOne);
            Sparrow sparrowTwo = new Sparrow(numTwo, numTwo, numTwo, numTwo);
            Sparrow sparrowThree = new Sparrow(numThree, numThree, numThree, numThree);

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            // Act
            Vector2 newVector = sparrowOne.Avoidance(flock);

            // Assert
            Assert.AreEqual(expected, newVector.Vx, 0.1);
            Assert.AreEqual(expected, newVector.Vy, 0.1);
        }

        // Speicific Test for Avoidance Method
        [TestMethod]
        public void AvoidanceSpecificTestOne(){
            // Arrange
            Sparrow sparrowOne = new Sparrow(-21, 1, 0, 0);
            Sparrow sparrowTwo = new Sparrow(-10, -7, 1, -3);
            Sparrow sparrowThree = new Sparrow(0, -37, -4, -1);

            float expectedX = -0.74f;
            float expectedY = 0.66f;

            List<Sparrow> flock = new List<Sparrow>() {sparrowOne, sparrowTwo, sparrowThree};

            // Act
            Vector2 newVector = sparrowOne.Avoidance(flock);

            // Assert
            Assert.AreEqual(expectedX, newVector.Vx, 0.01);
            Assert.AreEqual(expectedY, newVector.Vy, 0.01);
        }
        
        // General Tests for FleeRaven Method
        [TestMethod]
        [DataRow(1, 2f, -2.1f)]
        [DataRow(5, 10f, -2.1f)]
        [DataRow(3, 2.5f, 2.1f)]
        [DataRow(18, 5, 2.1f)]
        public void FleeRavenTests(int sparrowPosition, float ravenPosition, float expected){
            // Arrange
            Sparrow sparrow = new Sparrow(sparrowPosition, sparrowPosition, sparrowPosition, sparrowPosition);
            Raven raven = new Raven(ravenPosition, ravenPosition, ravenPosition, ravenPosition);

            // Act
            Vector2 newVector = sparrow.FleeRaven(raven);

            // Assert
            Assert.AreEqual(expected, newVector.Vx, 0.1);
            Assert.AreEqual(expected, newVector.Vy, 0.1);
        }

        // Specific Tests for FleeRaven Method
        [TestMethod]
        public void FleeRavenSpecificTestOne(){
            // Arrange
            Sparrow sparrow = new Sparrow(1, 1, 1, 1);
            Raven raven = new Raven(5, 10, -3, -4);

            float expectedX = -1.22f;
            float expectedY = -2.74f;

            // Act
            Vector2 newVector = sparrow.FleeRaven(raven);

            // Assert
            Assert.AreEqual(expectedX, newVector.Vx, 0.01);
            Assert.AreEqual(expectedY, newVector.Vy, 0.01);
        }

    }
    
}