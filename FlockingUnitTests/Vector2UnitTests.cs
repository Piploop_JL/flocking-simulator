using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using FlockingBackend;

namespace FlockingUnitTests{

    [TestClass]
    public class Vector2UnitTests{

        [TestMethod]
        [DataRow(3, 10, 6, 2, 9, 12)] // expectedX = 3 + 6 = 9  & expectedY = 12 + 2 = 12
        [DataRow(-4, 1, 6, -10, 2, -9)] // expectedX =  -4 + 6 = 2 & expectedY = 1 + -10 = -9
        ///<summary>
        /// Addition ( + ) Operator Overload of Vector2
        ///</summary>
        public void VectorAdditionTests(float vectorOneVx, float vectorOneVy, float vectorTwoVx, float vectorTwoVy, float expectedX, float expectedY){
            // Arrange
            Vector2 vectorOne = new Vector2(vectorOneVx, vectorOneVy);
            Vector2 vectorTwo = new Vector2(vectorTwoVx, vectorTwoVy);

            // Act
            Vector2 vectorThree = vectorOne + vectorTwo;

            // Assert
            Assert.AreEqual(expectedX, vectorThree.Vx);
            Assert.AreEqual(expectedY, vectorThree.Vy);
        }
        
        [TestMethod]
        [DataRow(3, 10, 6, 2, -3, 8)] // expectedX = 3 - 6 = -3  & expectedY = 10 - 2 = 8
        [DataRow(-4, 1, 6, -10, -10, 11)] // expectedX =  -4 - 6 = -10 & expectedY = 1 - -10 = 11
        ///<summary>
        /// Substraction ( - ) Operator Overload of Vector2
        ///</summary>
        public void VectorSubstractionTests(float vectorOneVx, float vectorOneVy, float vectorTwoVx, float vectorTwoVy, float expectedX, float expectedY){
            // Arrange
            Vector2 vectorOne = new Vector2(vectorOneVx, vectorOneVy);
            Vector2 vectorTwo = new Vector2(vectorTwoVx, vectorTwoVy);

            // Act
            Vector2 vectorThree = vectorOne - vectorTwo;

            // Assert
            Assert.AreEqual(expectedX, vectorThree.Vx);
            Assert.AreEqual(expectedY, vectorThree.Vy);
        }

        [TestMethod]
        [DataRow(40, -32, -8, -5, 4)] // expectedX = 40 / -8 = -5  & expectedY = -32 / -8 = 5
        [DataRow(33, 9, 3, 11, 3)] // expectedX =  33 / 3 = 11 & expectedY = 9 / 3 = 3
        ///<summary>
        /// Division ( / ) Operator Overload of Vector2
        ///</summary>
        public void VectorAndScalarDivisionTests(float vectorOneVx, float vectorOneVy, float scalar, float expectedX, float expectedY){
            // Arrange
            Vector2 vectorOne = new Vector2(vectorOneVx, vectorOneVy);

            // Act
            Vector2 vectorThree = vectorOne / scalar;

            // Assert
            Assert.AreEqual(expectedX, vectorThree.Vx);
            Assert.AreEqual(expectedY, vectorThree.Vy);
        }

        [TestMethod]
        [DataRow(4, -3, -8, -32, 24)] // expectedX = 3 * 5 = 15 & expectedY = -3 * -8 = 24
        [DataRow(3, 10 , 5 , 15, 50)] // expectedX = 4 * -8 = -32 & expectedY = -3 * -8 = 24
        ///<summary>
        /// Multiplication ( * ) Operator Overload of Vector2 | Vector * Scalar
        ///</summary>
        public void VectorScalarMultiplicationTests(float vectorOneVx, float vectorOneVy, float scalar, float expectedX, float expectedY){
            // Arrange
            Vector2 vectorOne = new Vector2(vectorOneVx, vectorOneVy);
            // Act
            Vector2 vectorThree = vectorOne * scalar;

            // Assert
            Assert.AreEqual(expectedX, vectorThree.Vx);
            Assert.AreEqual(expectedY, vectorThree.Vy);
        }

        [TestMethod]
        [DataRow(3, 11, 4, 2, 82)] // expectedSquareDistance = (3-4)^2 + (11-2)^2 = (-1)^2 + 9^2 = 1 + 81 = 82
        [DataRow(10, 5, 1, 20, 306)] // expectedSquareDistance = (10-1)^2 + (5-20)^2 = 9^2 + (-15)^2 = 81 + 225 = 306
        [DataRow(-6, 21, 14, -7, 1184)] // expectedSquareDistance = (-6-14)^2 + (21-(-7))^2 = (-20)^2 + 28^2 = 400 + 784 = 1184
        ///<summary>
        /// Calculation of the squared distance between two vectors | Test One
        ///</summary>
        public void DistanceSquaredTestOne(float vectorOneVx, float vectorOneVy, float vectorTwoVx, float vectorTwoVy, float expectedSquareDistance){
            // Arrange
            Vector2 vectorOne = new Vector2(vectorOneVx, vectorOneVy);
            Vector2 vectorTwo = new Vector2(vectorTwoVx, vectorTwoVy);

            // Act
            float squaredDistance = Vector2.DistanceSquared(vectorOne , vectorTwo);

            // Assert
            Assert.AreEqual(expectedSquareDistance, squaredDistance);
        }

        [TestMethod]
        ///<summary>
        /// Calculation of the squared distance between two vectors | Test Two
        ///</summary>
        public void DistanceSquaredTestTwo(){
            // Arrange
            Vector2 vectorOne = new Vector2(10, 5);
            Vector2 vectorTwo = new Vector2(1, 20);
            float expectedSquareDistance = 306; // (10-1)^2 + (5-20)^2 = 9^2 + (-15)^2 = 81 + 225 = 306

            // Act
            float squaredDistance = Vector2.DistanceSquared(vectorOne , vectorTwo);

            // Assert
            Assert.AreEqual(expectedSquareDistance, squaredDistance);
        }

        [TestMethod]
        ///<summary>
        /// Calculation of the squared distance between two vectors | Test Three
        ///</summary>
        public void DistanceSquaredTestThree(){
            // Arrange
            Vector2 vectorOne = new Vector2(-6, 21);
            Vector2 vectorTwo = new Vector2(14, -7);
            float expectedSquareDistance = 1184; // (-6-14)^2 + (21-(-7))^2 = (-20)^2 + 28^2 = 400 + 784 = 1184

            // Act
            float squaredDistance = Vector2.DistanceSquared(vectorOne , vectorTwo);

            // Assert
            Assert.AreEqual(expectedSquareDistance, squaredDistance);
        }

        [TestMethod]
        [DataRow(2 , 2)]
        [DataRow(-5 , 13)]
        ///<summary>
        /// Calculation of the normalized value of a vector
        ///</summary>
        public void NormalizeVectorTests(float xInput, float yInput){
            // Arrange
            Vector2 vector = new Vector2(xInput, yInput);
            float expectedMagnitude = (float) Math.Sqrt(vector.Vx * vector.Vx + vector.Vy * vector.Vy);
            float expectedNormalizedVectorX = vector.Vx / expectedMagnitude;
            float expectedNormalizedVectorY = vector.Vy / expectedMagnitude;

            // Act
            Vector2 normalizedVector = Vector2.Normalize(vector);

            // Assert
            Assert.AreEqual(expectedNormalizedVectorX, normalizedVector.Vx);
            Assert.AreEqual(expectedNormalizedVectorY, normalizedVector.Vy);
        }

        [TestMethod]
        [DataRow(2 , 2)]
        [DataRow(-5 , 13)]
        ///<summary>
        /// Check if two vectors with the same Vx and Vy are considered equal
        ///</summary>
        public void VectorAdditionTest(float xInput, float yInput){
            // Arrange
            Vector2 vectorOne = new Vector2(xInput, yInput);
            Vector2 vectorTwo = new Vector2(xInput, yInput);
            float expectedX = xInput + xInput;
            float expectedY = yInput + yInput;

            // Act
            vectorOne += vectorTwo;

            // Assert
            Assert.AreEqual(expectedX, vectorOne.Vx);
            Assert.AreEqual(expectedY, vectorOne.Vy);
        }

    }

}