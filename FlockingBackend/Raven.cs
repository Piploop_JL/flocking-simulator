using System;
using System.Collections.Generic;

namespace FlockingBackend{

    ///<summary>
    /// This class is used to represent a single raven. 
    ///</summary>
    public class Raven: Bird{

        ///<summary>
        /// Raven Default Constructor that uses the Bird Default Constructor
        ///</summary>
        public Raven(){}

        ///<summary>
        /// Raven Testing Constructor that uses the Bird Testing Constructor
        ///</summary>
        public Raven(float positionXInput, float positionYInput, float velocityXInput, float velocityYInput)
            :base(positionXInput, positionYInput, velocityXInput, velocityYInput){
        }

        ///<value> Property <c>Rotation</c> to rotate the raven to face the direction it is moving toward.</value>
        public override float Rotation
        {
            get 
            {
                return (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx); 
            }
        }

        ///<summary>
        /// This method is an event handler to calculate and set amountToSteer vector
        ///</summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows) 
        {
            this.amountToSteer = ChaseSparrow(sparrows);
        }

        ///<summary>
        /// ChaseSparrow Method that tries makes the Raven chase a Sparrow in the surrounding radius
        ///</summary>
        ///<param name="sparrows">
        /// Full list of sparrows in the World
        ///</param>
        ///<return>
        /// Returns the result vector that would change the Position of the Raven when updating the amountToSteer 
        ///</return>
        public Vector2 ChaseSparrow (List<Sparrow> sparrows){

                Vector2 nearestSparrow = new Vector2(0, 0);

                foreach (var sparrow in sparrows){
                    float currentSquaredDistance = Vector2.DistanceSquared(this.Position, sparrow.Position);

                    if(currentSquaredDistance < Math.Pow(World.AvoidanceRadius, 2)){
                        nearestSparrow = sparrow.Position;
                    }
                }

            Vector2 calculatedDifference = new Vector2(0, 0);

            if(nearestSparrow.Vx != 0 && nearestSparrow.Vy != 0){
                calculatedDifference = nearestSparrow - this.Position;
            }

            return calculatedDifference;
        }

        ///<summary>
        /// Overriden Move Method from Bird to normalize the velocity of the Raven and multiply it by the World.MaxSpeed
        ///</summary>
        public override void Move(){
            this.Velocity += amountToSteer;
            this.Velocity = Vector2.Normalize(Velocity) * World.MaxSpeed;
            this.Position += Velocity;
        }
        
    }
    
}