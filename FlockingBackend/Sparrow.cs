using System;
using System.Collections.Generic;

namespace FlockingBackend{

    ///<summary>
    /// This class is used to represent a single sparrow. 
    ///</summary>
    public class Sparrow: Bird{

        ///<summary>
        /// Sparrow Default Constructor that uses the Bird Default Constructor
        ///</summary>
        public Sparrow(){}

        ///<summary>
        /// Sparrow Testing Constructor that uses the Bird Testing Constructor
        ///</summary>
        public Sparrow(float positionXInput, float positionYInput, float velocityXInput, float velocityYInput)
            :base(positionXInput, positionYInput, velocityXInput, velocityYInput){
        }

        ///<value> Property <c>Rotation</c> to rotate the Sparrow to face the direction it is moving toward.</value>
        public override float Rotation{
            get 
            {
                return (float)Math.Atan2(this.Velocity.Vy, this.Velocity.Vx); 
            }
        }

        ///<summary>
        /// This method is an event handler to calculate and set amountToSteer vector using the flocking algorithm rules
        ///</summary>
        ///<param name="sparrows">List of sparrows</param>
        public override void CalculateBehaviour(List<Sparrow> sparrows) 
        {
            this.amountToSteer = Alignment(sparrows) + Cohesion(sparrows) + Avoidance(sparrows);
        }

        ///<summary>
        /// This method is an event handler to calculate and update amountToSteer vector with the amount to steer to flee a chasing raven
        ///</summary>
        ///<param name="raven">A Raven object</param>
        public void CalculateRavenAvoidance(Raven raven)
        {
            amountToSteer += FleeRaven(raven);
        }
        
        ///<summary>
        /// Alignment Method that tries to align the current sparrow with the flock in its neighbouring radius
        ///</summary>
        ///<param name="sparrows">
        /// Full list of sparrows in the World
        ///</param>
        ///<return>
        /// Returns the result vector that would change the Velocity of the Sparrow when updating the amountToSteer
        ///</return>
        public Vector2 Alignment (List<Sparrow> sparrows){

            Vector2 totalVelocity = new Vector2(0, 0);
            int count = 0;

            foreach (var sparrow in sparrows){
                if(this != sparrow){
                    float calculatedSquaredDistance = Vector2.DistanceSquared(this.Position, sparrow.Position);

                    if(calculatedSquaredDistance < Math.Pow(World.NeighbourRadius, 2)){
                        totalVelocity += sparrow.Velocity;
                        count++;
                    }

                }
            }

            return calculateNormalizedAverage(totalVelocity, count, new Vector2(0, 0));
        }
        
        ///<summary>
        /// Cohesion Method that tries to gather the current sparrow with the rest of the flock in its neighbouring radius
        ///</summary>
        ///<param name="sparrows">
        /// Full list of sparrows in the World
        ///</param>
        ///<return>
        /// Returns the result vector that would change the Position of the Sparrow when updating the amountToSteer
        ///</return>
        public Vector2 Cohesion (List<Sparrow> sparrows){

            Vector2 totalPosition = new Vector2(0, 0);
            int count = 0;

            foreach (var sparrow in sparrows){
                if(this != sparrow){
                    float calculatedSquaredDistance = Vector2.DistanceSquared(this.Position, sparrow.Position);

                    if(calculatedSquaredDistance < Math.Pow(World.NeighbourRadius, 2)){
                        totalPosition += sparrow.Position;
                        count++;
                    }

                }
            }

            Vector2 average = new Vector2(0, 0);
            Vector2 normalized = new Vector2(0, 0);

            return calculateNormalizedAverage(totalPosition, count, this.Position);
        }

        ///<summary>
        /// Avoidance Method that slightly spread the sparrow from the neighbour sparrows so it does not converge on the same Position from Cohesion
        ///</summary>
        ///<param name="sparrows">
        /// Full list of sparrows in the World
        ///</param>
        ///<return>
        /// Returns the result vector that would change the Position of the Sparrow when updating the amountToSteer
        ///</return>
        public Vector2 Avoidance (List<Sparrow> sparrows){

            Vector2 resultVector = new Vector2(0, 0);
            int count = 0;

            foreach (var sparrow in sparrows){
                if(this != sparrow){
                    float calculatedSquaredDistance = Vector2.DistanceSquared(this.Position, sparrow.Position);

                    if(calculatedSquaredDistance < Math.Pow(World.NeighbourRadius, 2)){
                        Vector2 positionDifference = this.Position - sparrow.Position;
                    
                        resultVector += (positionDifference / calculatedSquaredDistance);
                        count++;
                    }
                }
            }

            return calculateNormalizedAverage(resultVector, count, new Vector2(0, 0));

        }
        
        ///<summary>
        /// FleeRaven Method that tries makes the Sparrow run away from the Raven
        ///</summary>
        ///<param name="raven">
        /// The raven that chases the sparrow...
        ///</param>
        ///<return>
        /// Returns the result vector that would change the Position of the Sparrow when updating the amountToSteer 
        ///</return>
        public Vector2 FleeRaven(Raven raven){
            Vector2 newVector = new Vector2(0, 0);

            float calculatedSquaredDistance = Vector2.DistanceSquared(this.Position, raven.Position);

            if(calculatedSquaredDistance < Math.Pow(World.AvoidanceRadius, 2)){
                newVector = this.Position - raven.Position;
                newVector = newVector / calculatedSquaredDistance;
                newVector = Vector2.Normalize(newVector) * World.MaxSpeed;
            }

            return newVector;
        }

        ///<summary>
        /// Helper Method for the Flocking Algorithm Rules
        /// Calculates the normalized average velocity vector
        /// Multiplies it by MaxSpeed to make the sparrows have the same magnitude
        /// Substract the sparrow's velocity from the average and normalize it again
        /// In order to align the sparrow's velocity with its neighbouring sparrows
        ///</summary>
        ///<param name="total">
        /// Total sum of the vectors to be the dividend of the average
        ///</param>
        ///<param name="count">
        /// Number of sparrows in the neighbouring/avoiding radius for the divisor of the average
        ///</param>
        ///<return>
        /// Returns the normalized average minus the Velocity of the sparrow
        ///</return>
        public Vector2 calculateNormalizedAverage(Vector2 total, int count, Vector2 substract){

            Vector2 average = new Vector2(0, 0);
            Vector2 normalized = new Vector2(0, 0);

            if(count > 0){
                average = total / count;
                normalized = Vector2.Normalize(average - substract);
                normalized = normalized * (float) World.MaxSpeed;
                normalized -= this.Velocity;
                normalized = Vector2.Normalize(normalized);
            }

            return normalized;

        }
        
    }

}