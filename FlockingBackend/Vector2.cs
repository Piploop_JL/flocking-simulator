using System;

namespace FlockingBackend{

    public struct Vector2{
        
        ///<summary>
        /// Vx Property | X Coordinate of the Vector
        ///</summary>
        public float Vx{
            get;
        }

        ///<summary>
        /// Vy Property | Y Coordinate of the Vector
        ///</summary>
        public float Vy{
            get;
        }

        ///<summary>
        /// Vector2 Constructor | Takes X and Y Coordinates to create the Vector
        ///</summary>
        ///<param name="xInput">
        /// X Coordinate Input 
        ///</param>
        ///<param name="yInput">
        /// Y Coordinate Input
        ///</param>
        public Vector2(float xInput, float yInput){
            this.Vx = xInput;
            this.Vy = yInput;
        }

        ///<summary>
        /// Calculates the sum of the Vx and Vy Properties of 2 Vector2s to create a resulting Vector2
        ///</summary>
        ///<param name="vectorOne">
        /// First Vector2 to add up
        ///</param>
        ///<param name="vectorTwo">
        /// Second Vector2 to add up
        ///</param>
        ///<return>
        /// Returns the result Vector2 from the sum of the inputed Vector2s
        ///</return>
        public static Vector2 operator +(Vector2 vectorOne, Vector2 vectorTwo){
            
            float newX = vectorOne.Vx + vectorTwo.Vx;
            float newY = vectorOne.Vy + vectorTwo.Vy;
            
            return new Vector2(newX, newY);
        }

        ///<summary>
        /// Calculates the difference of the Vx and Vy Properties of 2 Vector2s to create a resulting Vector2
        ///</summary>
        ///<param name="vectorOne">
        /// First Vector2 to substract from
        ///</param>
        ///<param name="vectorTwo">
        /// Second Vector2 to substract
        ///</param>
        ///<return>
        /// Returns the result Vector2 from the difference of the inputed Vector2s
        ///</return>
        public static Vector2 operator -(Vector2 vectorOne, Vector2 vectorTwo){
            float newX = vectorOne.Vx - vectorTwo.Vx;
            float newY = vectorOne.Vy - vectorTwo.Vy;
            
            return new Vector2(newX, newY);
        }

        ///<summary>
        /// Calculates the quotient of the Vx and Vy Properties of a Vector2 and a scalar to create a resulting Vector2
        ///</summary>
        ///<param name="vector">
        /// Vector whose coordinates are going to be the dividend
        ///</param>
        ///<param name="scalar">
        /// Scalar that is going to be the divisor
        ///</param>
        ///<return>
        /// Returns the resulting Vector2 from the division of the inputed Vector2 and scalar
        ///</return>
        public static Vector2 operator /(Vector2 vector, float scalar){
            float newX = vector.Vx / scalar;
            float newY = vector.Vy / scalar;
            
            return new Vector2(newX, newY);
        }

        ///<summary>
        /// Calculates the product of the Vx and Vy Properties of a Vector2 with a scalar to create a resulting Vector2
        ///</summary>
        ///<param name="vector">
        /// Vector whose coordinates are going to be the factors
        ///</param>
        ///<param name="scalar">
        /// Scalar that is going to be another factor
        ///</param>
        ///<return>
        /// Returns the resulting Vector2 from the multiplication of the inputed Vector2 and scalar
        ///</return>
        public static Vector2 operator *(Vector2 vector, float scalar){
            float newX = vector.Vx * scalar;
            float newY = vector.Vy * scalar;
            
            return new Vector2(newX, newY);
        }
        
        ///<summary>
        /// Find the distance between two Vector2s
        ///</summary>
        ///<param name="vectorOne">
        /// First Vector2 Input
        ///</param>
        ///<param name="vectorTwo">
        /// Second Vector2 Input
        ///</param>
        ///<return>
        /// Returns the squared distance between the two inputed Vector2s
        ///</return>
        public static float DistanceSquared(Vector2 vectorOne, Vector2 vectorTwo){

            float squaredDistance = (vectorOne.Vx - vectorTwo.Vx) * (vectorOne.Vx - vectorTwo.Vx) + (vectorOne.Vy - vectorTwo.Vy) * (vectorOne.Vy - vectorTwo.Vy);
            // Note we can simply remove Math.Sqrt from the formula to find the squared distance since SquareRoot and Squared cancels each other

            return squaredDistance;
        }

        ///<summary>
        /// Normalize the inputed Vector2sr to create a new Vector2
        /// Find the magnitude of the vector, then divide the vector's coordinate to find the new Vector2
        ///</summary>
        ///<param name="vector">
        /// Vector to be normalized
        ///</param>
        ///<return>
        /// Returns the resulting Vector2 from the normalization
        ///</return>
        public static Vector2 Normalize(Vector2 vector){
            float magnitude = (float) Math.Sqrt(vector.Vx * vector.Vx + vector.Vy * vector.Vy);

            float newX = vector.Vx / magnitude;
            float newY = vector.Vy / magnitude;

            return new Vector2(newX, newY);
        }

    }

}