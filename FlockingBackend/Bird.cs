using System;
using System.Collections.Generic;

namespace FlockingBackend{

    public abstract class Bird{

        ///<summary>
        /// The last Velocity of the bird
        ///</summary>
        public Vector2 LastVelocity{
            get; private set;
        }

        ///<summary>
        /// The last Velocity of the bird
        /// used to make the bird image jitter less
        ///</summary>
        public Vector2 BeforeLastVelocity{
            get; private set;
        }
        
        
        ///<summary>
        /// Rotation Property
        ///</summary>
        public virtual float Rotation{
            get;
        }

        ///<summary>
        /// This method is a private helper method to make sparrows reappear on the opposite edge if they go outside the bounds of the screen
        ///</summary>
        public virtual void AppearOnOppositeSide(){
    
           if (this.Position.Vx > World.Width)
            {
                this.Position = new Vector2(0, this.Position.Vy);
            }
            else if(this.Position.Vx < 0)
            {
                 this.Position = new Vector2(World.Width, this.Position.Vy);
            }
            if (this.Position.Vy > World.Height)
            {
                this.Position = new Vector2(this.Position.Vx, 0);
            }
            else if(this.Position.Vy < 0)
            {
                this.Position= new Vector2(this.Position.Vx, World.Height);
            }
        }

        ///<summary>
        /// Position Property | Position of the Bird
        ///</summary>
        ///<return>
        /// Returns the Vector2 indicating the Bird's Position
        ///</return>
        public Vector2 Position{
            get; protected set;
        }

        ///<summary>
        /// Velocity Property | Velocity of the Bird
        ///</summary>
        ///<return>
        /// Returns the Vector2 indicating the Bird's Velocity
        ///</return>
        public Vector2 Velocity{
            get; protected set;
        }

        ///<summary>
        /// Vector2 amountToSteer field | Holds the Vector2 value that determines the new direction of the Bird after flocking rules applied
        ///</summary>
        protected Vector2 amountToSteer;

        ///<summary>
        /// Bird Default Constructor
        ///</summary>
        public Bird(){
            Random rand = new Random();

            float randomPositionX = rand.Next(0, World.Width + 1);
            float randomPositionY = rand.Next(0, World.Height + 1);

            this.Position = new Vector2(randomPositionX, randomPositionY);

            float randomVelocityX = rand.Next(-World.MaxSpeed, World.MaxSpeed + 1);
            float randomVelocityY = rand.Next(-World.MaxSpeed, World.MaxSpeed + 1);

            this.Velocity = new Vector2(randomVelocityX, randomVelocityY);

            this.amountToSteer = new Vector2(0 ,0);
        }

        ///<summary>
        /// Bird Testing Constructor
        ///</summary>
        public Bird(float positionXInput, float positionYInput, float velocityXInput, float velocityYInput){
            this.Position = new Vector2(positionXInput, positionYInput);

            this.Velocity = new Vector2(velocityXInput, velocityYInput);

            this.amountToSteer = new Vector2(0 ,0);
        }

        ///<summary>
        /// Abstract Method that calculates how the Bird will move
        ///</summary>
        public abstract void CalculateBehaviour(List<Sparrow> sparrowList);

        ///<summary>
        /// Move method that adjusts the Velocity and Position based on the amountToSteer calculated from CalculateBehaviour
        ///</summary>
        public virtual void Move(){
            this.BeforeLastVelocity = this.LastVelocity;
            this.LastVelocity = this.Velocity;
            this.Velocity += amountToSteer;
            this.Position += Velocity;
        }

    }

}