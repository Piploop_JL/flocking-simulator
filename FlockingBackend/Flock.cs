using System;
using System.Collections.Generic;

namespace FlockingBackend
{
    ///<summary>
    /// This class is the subscriber class that each bird subscribes to. The class also raises the events to calculate movement vector and move the birds.
    ///</summary>
    public class Flock
    {

        ///<summary>
        /// Public events to store the delegates that will affect the movement of the birds
        ///</summary>
        public event CalculateMoveVector CalcMovementEvent;
        public event CalculateRavenAvoidance CalcRavenFleeEvent;
        public event MoveBird MoveEvent;

        ///<summary>
        /// This method raises the calculate and move events
        ///</summary>
        ///<param name="sparrows">List of Sparrow objects</param>
        ///<param name="raven">A Raven object</param>
        public void RaiseMoveEvents(List<Sparrow> sparrows, Raven raven)
        {
            CalcMovementEvent?.Invoke(sparrows);
            CalcRavenFleeEvent?.Invoke(raven);
            MoveEvent?.Invoke();
        }
        
        ///<summary>
        /// This method assigns all the delegates into the designated events
        ///</summary>
        ///<param name="moveVector">Delegate object</param>
        ///<param name="moveBird">Delegate object </param>
        ///<param name="avoidRaven">Delegate object, is optional </param>
        public void Subscribe(CalculateMoveVector moveVector, MoveBird moveBird, CalculateRavenAvoidance avoidRaven = null){

            if(moveVector != null){
                CalcMovementEvent += moveVector;
            }
            if(moveBird != null){
                MoveEvent += moveBird;
            }
            if(avoidRaven != null){
                CalcRavenFleeEvent += avoidRaven;
            }

        }
        
    }
    
}