using System.Collections.Generic;

namespace FlockingBackend{

    /// <summary>
    /// This class contains the fields and methods related to the world/environment where the birds reside.
    /// </summary>
    public class World{

        // Fields and Properties
        // Flock object
        private Flock flock;

        // List of sparrows
        public List<Sparrow> Sparrows{ get; }

        // Raven Object
        public Raven RavenBird{ get; }

        // Static Fields
        // The amount of Sparrows
        public static int InitialCount{ get; }
        // Width of the canvas
        public static int Width{ get; }
        // Height of the canvas
        public static int Height{ get; }
        // Max speed of the birds
        public static int MaxSpeed{ get; }
        // Radius used to determine if a bird is a neighbour
        public static double NeighbourRadius{ get; }
        // Radius used to determine if a bird is too close
        public static double AvoidanceRadius{ get; }

        /// <summary>
        /// This is a static constructor that will initialize all static fields and properties
        /// </summary>
        static World(){
            InitialCount = 150;
            Width = 1000;
            Height = 500;
            MaxSpeed = 3;
            NeighbourRadius = 100.0;
            AvoidanceRadius = 50.0;
        }

        /// <summary>
        /// World Constructor that will initialize the flock field and the Sparrows property
        /// </summary>
        public World(){
            flock = new Flock();
            Sparrows = GenerateListOfSparrows();
            RavenBird = GenerateRaven();

        }

        /// <summary>
        /// Initializes and adds InitialCount amount of sparrows into a List of Sparrows
        /// </summary>
        /// <returns>
        /// List<Sparrow> object
        /// </returns>
        private List<Sparrow> GenerateListOfSparrows(){
            // List of Sparrows
            List<Sparrow> tempList = new List<Sparrow>();
            // Sparrow object that will be added to the list
            Sparrow tempSparrow;

            // Generates a new Sparrow for the InitialCount amount, and adds to the list
            for (int i = 0; i < InitialCount; i++){
                tempSparrow = new Sparrow();

                // Subscribe the Sparrow to the flock
                flock.Subscribe(tempSparrow.CalculateBehaviour, tempSparrow.Move, tempSparrow.CalculateRavenAvoidance);
                tempList.Add(tempSparrow);
            }
            return tempList;
        }

        /// <summary>
        /// Creates a Raven object and Subscribes it to the Flock.
        /// </summary>
        /// <returns>
        /// Raven object
        /// </returns>
        private Raven GenerateRaven(){
            //Create a new temporary Raven object
            Raven tempRaven = new Raven();

            //Subscribe the raven to the Flock
            flock.Subscribe(tempRaven.CalculateBehaviour, tempRaven.Move);
            return tempRaven;
        }

        /// <summary>
        /// Invoke the flock's RaiseMoveEvents while passing the List of Sparrows and Raven objects to it
        /// </summary>
        public void Update(){
            flock.RaiseMoveEvents(Sparrows, RavenBird);
        }

    }

}