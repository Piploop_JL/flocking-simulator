using System.Collections.Generic;

namespace FlockingBackend{
    
    /// <summary>
    /// A delegate object that will raise the event to calculate the movement vector for each bird
    /// </summary>
    /// <param name="sparrows">
    /// The list of sparrows
    /// </param>
    public delegate void CalculateMoveVector(List<Sparrow> sparrows);

    /// <summary>
    /// A delegate object that will raise an event that will move a bird
    /// </summary>
    public delegate void MoveBird();

    /// <summary>
    /// A delegate object that will raise an event that will calculate the vector amount to flee the raven
    /// </summary>
    /// <param name="raven"></param>
    public delegate void CalculateRavenAvoidance(Raven raven);

}