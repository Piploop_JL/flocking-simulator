using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using FlockingBackend;

namespace FlockingSimulation{

    /// <summary>
    /// This class is used to manipulate information and display of a sparrow
    /// </summary>
    public class SparrowFlockSprite : DrawableGameComponent{
        private Game1 game;
        private SpriteBatch spriteBatch;

        //list of sparrows
        private List<Sparrow> sparrows;

        //textures
        private Texture2D sparrowDown;
        private Texture2D sparrowRight;
        private Texture2D sparrowFlap;
        private Texture2D sparrowFlapDown;
        private Texture2D sparrowFront;

        //counter used for the animation
        private int flapCounter;
        private const int flapLimit = 20;
        private bool flapState;

        /// <summary>
        /// Constructor for SparrowFlockSprite
        /// </summary>
        /// <param name="game">
        /// Game Object
        /// </param>
        public SparrowFlockSprite(Game1 game, List<Sparrow> sparrows) : base(game){
            this.game = game;
            this.sparrows = sparrows;
            flapCounter = 0;
        }

        /// <summary>
        /// Load the images needed for this sprite
        /// </summary>
        protected override void LoadContent(){
            spriteBatch = new SpriteBatch(GraphicsDevice);
            sparrowDown = game.Content.Load<Texture2D>("sparrowDown");
            sparrowRight = game.Content.Load<Texture2D>("sparrowRight");
            sparrowFlap = game.Content.Load<Texture2D>("sparrowFlap");
            sparrowFlapDown = game.Content.Load<Texture2D>("sparrowFlapDown");
            sparrowFront = game.Content.Load<Texture2D>("sparrowFront");

            base.LoadContent();
        }

        /// <summary>
        /// Updates the status of the bird,
        /// Makes sure that the bird will reappear on the other side of the screen when it flies out of bounds
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime){

            foreach(Bird sparrow in sparrows){
                sparrow.AppearOnOppositeSide();
            }

            base.Draw(gameTime);
        } 

        /// <summary>
        /// Draw each sparrow on screen
        /// </summary>
        /// <param name="gameTime">
        /// GameTime object
        /// </param>
        public override void Draw(GameTime gameTime){
            spriteBatch.Begin();

            Texture2D sparrowTexture = sparrowFront;
            
            // check if the counter reaches the limit to switch the flap state and reset the counter
            if(flapCounter == flapLimit){
                flapState = !flapState;
                flapCounter = 0;
            }
            
            foreach(Sparrow sparrow in sparrows){
                // if the bird is flapping, use the appropriate flapping images
                if (flapState)
                {
                    // going right and down (+, +) or going right and up (+, -)
                    // Compares the previous velocities to determine if a bird actually wants to go in the direction it is heading towards
                    // instead of constantly changing images to match it switching directions
                    // This will help reduce the jitter
                    if (sparrow.Velocity.Vx > 0 || sparrow.LastVelocity.Vx > 0 && sparrow.BeforeLastVelocity.Vx > 0.9)
                    {
                        sparrowTexture = sparrowFlap;
                    }
                    // going left and down (-, +) or going left and up (-, -)
                    else if (sparrow.Velocity.Vx < 0 || sparrow.LastVelocity.Vx < 0 && sparrow.BeforeLastVelocity.Vx < 0.9)
                    {
                        sparrowTexture = sparrowFlapDown;
                    } 
                }
                else{
                    // going right and down (+, +) or going right and up (+, -)
                    // Compares the previous velocities to determine if a bird actually wants to go in the direction it is heading towards
                    // instead of constantly changing images to match it switching directions
                    // This will help reduce the jitter
                    if (sparrow.Velocity.Vx > 0 || sparrow.LastVelocity.Vx > 0 && sparrow.BeforeLastVelocity.Vx > 0.9)
                    {
                        sparrowTexture = sparrowRight;
                    }
                    // going left and down (-, +) or going left and up (-, -)
                    else if (sparrow.Velocity.Vx < 0 || sparrow.LastVelocity.Vx < 0 && sparrow.BeforeLastVelocity.Vx < 0.9)
                    {
                        sparrowTexture = sparrowDown;
                    }
                }      
                spriteBatch.Draw(sparrowTexture, new Microsoft.Xna.Framework.Vector2(sparrow.Position.Vx, sparrow.Position.Vy), null, Color.White, sparrow.Rotation, new Microsoft.Xna.Framework.Vector2(10, 10), 1, SpriteEffects.None, 0f);
            }

            // increase the flap counter
            flapCounter++;
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }

    /// <summary>
    /// This class will manipulate information and display a Raven
    /// </summary>
    public class RavenSprite : DrawableGameComponent{

        private Game1 game;
        private SpriteBatch spriteBatch;

        //raven object
        private Raven raven;

        //textures
        private Texture2D ravenDown;
        private Texture2D ravenRight;
        private Texture2D ravenFlap;
        private Texture2D ravenFlapDown;
        private Texture2D ravenFront;
        
        //counter used for animations
        private int flapCounter;
        private const int flapLimit = 5;
        private bool flapState;

        /// <summary>
        /// Constructor for the RavenSprite
        /// </summary>
        /// <param name="game">Game1 object</param>
        /// <param name="raven">Raven object</param>
        public RavenSprite(Game1 game, Raven raven) : base(game){
            this.game = game;
            this.raven = raven;
        }

        /// <summary>
        /// Loads the textures for the Raven
        /// </summary>
        protected override void LoadContent(){
            spriteBatch = new SpriteBatch(GraphicsDevice);

            ravenDown = game.Content.Load<Texture2D>("ravenDown");
            ravenRight = game.Content.Load<Texture2D>("ravenRight");
            ravenFlap = game.Content.Load<Texture2D>("ravenFlap");
            ravenFlapDown = game.Content.Load<Texture2D>("ravenDownFlap");
            ravenFront = game.Content.Load<Texture2D>("ravenFront");
            spriteBatch = new SpriteBatch(GraphicsDevice);
 
            base.LoadContent();
        }
        /// <summary>
        /// Updates the status of the raven
        /// If it goes out of bounds, replace it in the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime){

            raven.AppearOnOppositeSide();
            base.Draw(gameTime);
        } 

        /// <summary>
        /// Draws the Raven on screen
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime){

            // check if the counter reaches the limit to switch the flap state and reset the counter
            if(flapCounter == flapLimit){
                flapState = !flapState;
                flapCounter = 0;
            }

            spriteBatch.Begin();

            // default texture for the raven
            Texture2D ravenTexture = ravenFront;

            // if the bird is flapping, use the appropriate flapping images
            if (flapState){
                // going right and down (+, +) or going right and up (+, -)
                // Compares the previous velocities to determine if a bird actually wants to go in the direction it is heading towards
                // instead of constantly changing images to match it switching directions
                // This will help reduce the jitter
                if (raven.Velocity.Vx > 0 || raven.LastVelocity.Vx > 0 && raven.BeforeLastVelocity.Vx > 0.9)
                {
                    ravenTexture = ravenFlap;
                }
                // going left and down (-, +) or going left and up (-, -)
                else if (raven.Velocity.Vx < 0 || raven.LastVelocity.Vx < 0 && raven.BeforeLastVelocity.Vx < 0.9)
                {
                    ravenTexture = ravenFlapDown;
                }
            }
            else{
                // going right and down (+, +) or going right and up (+, -)
                // Compares the previous velocities to determine if a bird actually wants to go in the direction it is heading towards
                // instead of constantly changing images to match it switching directions
                // This will help reduce the jitter
                if (raven.Velocity.Vx > 0 || raven.LastVelocity.Vx > 0 && raven.BeforeLastVelocity.Vx > 0.9)
                {
                    ravenTexture = ravenRight;
                }
                // going left and down (-, +) or going left and up (-, -)
                else if (raven.Velocity.Vx < 0 || raven.LastVelocity.Vx < 0 && raven.BeforeLastVelocity.Vx < 0.9)
                {
                    ravenTexture = ravenDown;
                }
            }
            
            // increase the flap counter
            flapCounter++;

            spriteBatch.Draw(ravenTexture, new Microsoft.Xna.Framework.Vector2(raven.Position.Vx, raven.Position.Vy), null, Color.White, raven.Rotation, new Microsoft.Xna.Framework.Vector2(10, 10), 1, SpriteEffects.None, 0f);
            spriteBatch.End();
        }

    }

}