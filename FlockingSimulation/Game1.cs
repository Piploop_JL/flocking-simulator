﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using FlockingBackend;

namespace FlockingSimulation
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        // background image texture
        private Texture2D background;

        // sprites
        private RavenSprite ravenSprite;
        private SparrowFlockSprite sparrowFlockSprite;

        // list of sparrows
        private List<Sparrow> sparrows;
        // world object
        private World world;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            world = new World();
        }

        /// <summary>
        /// Initialize the Sprites and Window size
        /// </summary>
        protected override void Initialize()    
        {
            sparrows = new List<Sparrow>();
            ravenSprite = new RavenSprite(this, world.RavenBird);
            sparrowFlockSprite = new SparrowFlockSprite(this, world.Sparrows);

            // Set the window to 1000x500
            _graphics.PreferredBackBufferHeight = 500;
            _graphics.PreferredBackBufferWidth = 1000;
            _graphics.ApplyChanges();

            Components.Add(ravenSprite);
            Components.Add(sparrowFlockSprite);

            base.Initialize();
        }

        /// <summary>
        /// Loads the Background image
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>("inazumaBg");
        }

        /// <summary>
        /// Updates the world, which invokes the necessary events
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            world.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// Displays the background image
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            _spriteBatch.Draw(background,  new Microsoft.Xna.Framework.Vector2(GraphicsDevice.Viewport.Bounds.Left,GraphicsDevice.Viewport.Bounds.Top), Color.White);

             _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
